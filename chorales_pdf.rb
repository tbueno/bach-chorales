load_library "pdf"
import "processing.pdf*"

SCALE_FACTOR = 80
HEIGHT = 1000
WIDTH = 1600
X_AXIS = HEIGHT/2 +100




def setup
  size WIDTH, HEIGHT
  #size(WIDTH, HEIGHT, PDF, File.expand_path(File.dirname(__FILE__))+"/chorales.pdf" )
  background 0,0,0
  #@bg = loadImage(File.expand_path(File.dirname(__FILE__))+"/images/Bach_big.jpg")
  #background @bg
  @arcs = []
  @current_choral = 0
  read_chorales
  update_notes @current_choral
  puts "#{@chorales.size}"
  font = createFont("SansSerif", 20)
  textFont font
end

def semicircle xLeft, yLeft, xRight, yRight
  pushMatrix
    diameter = dist xLeft, yLeft, xRight, yRight
    angle = atan2(yRight - yLeft, xRight - xLeft)
    arc((xLeft + xRight)/2, (yLeft + yRight) / 2, diameter, diameter, angle, angle + PI)
  popMatrix
end


def draw
  background 0,0,0
  #background @bg
  draw_labels
  @arcs.each do |ar|
    noFill
    stroke(ar.c)
    smooth
    semicircle(ar.st+ar.duration, X_AXIS, ar.st,X_AXIS )
    line(ar.st, X_AXIS+10, ar.st, X_AXIS+10+ar.pitch)
    line(ar.st+ar.duration, X_AXIS+10, ar.st+ar.duration, X_AXIS+10+ar.pitch)
    
  end
  exit
  noLoop
  
end

def draw_labels
  fill 255
  textSize 10
  textAlign CENTER, TOP
  stroke 224
  strokeWeight 1
  @arcs.each do |ar|
    text("#{ar.st+ar.duration}", ar.st+ar.duration, X_AXIS+5)
  end
end

def update_notes(current)
  @notes = @chorales[current]
  @arcs = []
  @notes.each do |note|
    @arcs << Arc.new(self, note.st, note.duration, note.pitch)
  end
end


def read_chorales
  f = File.open('bach.lisp')
  lines = []
  File.open("bach.lisp", "r") do |infile|
       while (line = infile.gets)
           lines << "#{line}" unless line.size == 1 #ignora linhas em branco
       end
  end
  @chorales = []
  
  lines.each do |choral|
    notes = []
    a = choral.split('st ')
    a[1..a.size-1].each do |st|
      notes << Note.new((st.split(')')[0]).to_i)
    end

    #pega os duration
    a = choral.split('dur ')
    a[1..a.size-1].each_with_index do |dur, index|  
      notes[index].duration = (dur.split(')')[0]).to_i * SCALE_FACTOR
    end
    
    #pega os tons
    a = choral.split('pitch ')
    a[1..a.size-1].each_with_index do |pitch, index|  
      notes[index].pitch = (pitch.split(')')[0]).to_i * (SCALE_FACTOR/50)
    end
    
    @chorales << notes
  end
  
end

def key_pressed
  if key == '['
    @current_choral-=1 unless @current_choral < 1    
  elsif key == ']'
    @current_choral+=1 unless @current_choral == @chorales.size-1
  end
  update_notes @current_choral
  puts "#{@current_choral} - #{key} "
end




class Arc
  
  attr_accessor :st, :duration, :c, :pitch
  
  def initialize(sketch, st, duration, pitch, c =nil)
    @st = st
    @duration = duration
    @pitch = pitch
    @c = sketch.color(sketch.random(255), rand(255), rand(255)) unless c
  end
    
end

class Note
  attr_accessor :st, :duration, :pitch

  def initialize(st ='', duration ='')
    @st = st
    @duration = duration
    @pitch = pitch
  end
end
