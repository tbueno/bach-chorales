class Note
  attr_accessor :st, :duration, :pitch

  def initialize(st ='', duration ='')
    @st = st
    @duration = duration
    @pitch = pitch       
  end
end
