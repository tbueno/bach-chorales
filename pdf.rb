load_library "pdf"
import 'processing.pdf'

def setup
  size(1200, 768, PDF, File.expand_path(File.dirname(__FILE__))+"/chorales.pdf" )

  background(255)
  fill(175)
  stroke(0)
  ellipse(width/2,height/2,160,160)

  exit
  
end
 