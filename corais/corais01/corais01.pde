PVector p1, p2, p3, p4;

String[] lines;


void setup(){
  size(800,600);
  background(255,255,255);
 
  lines = loadStrings("notas.txt");
  
}

void semicircle(float xLeft, float yLeft, float xRight, float yRight) {
  
  pushMatrix();
   float diameter = dist(xLeft, yLeft, xRight, yRight);
   float angle = atan2(yRight - yLeft, xRight - xLeft); 
   arc((xLeft + xRight) / 2, (yLeft + yRight) / 2, diameter, diameter, angle, angle + PI); 
  popMatrix(); 
}

void draw(){ 
// semicircle(p4.x, p4.y, p1.x, p1.y);
// semicircle(p3.x, p3.y, p2.x, p2.y);
 for(int i=0;i<lines.length;i++){
    String[] pieces = split(lines[i], ',');
    if (pieces.length == 2) {
      int st = int(pieces[0]);
      int duration = 20 * int(pieces[1]);
      noFill();
      semicircle(st+duration, height/2, st, height/2 );
      //println(x+"-"+y);     
    }
  }
}

