PVector p1, p2, p3, p4;

String[] lines;
Arc[] arcs;

int SCALE_FACTOR = 50;

void setup(){
  size(800,600);
  background(0,0,0); 
  lines = loadStrings("notas.txt");
  arcs = new Arc[lines.length];
  for(int i=0;i<lines.length;i++){
    String[] pieces = split(lines[i], ',');
    if (pieces.length == 2) {
      int st = int(pieces[0]);
      int duration = SCALE_FACTOR * int(pieces[1]);
      arcs[i] = new Arc(st, duration);
    }
  }
  
}

void semicircle(float xLeft, float yLeft, float xRight, float yRight) {
  
  pushMatrix();
   float diameter = dist(xLeft, yLeft, xRight, yRight);
   float angle = atan2(yRight - yLeft, xRight - xLeft); 
   arc((xLeft + xRight) / 2, (yLeft + yRight) / 2, diameter, diameter, angle, angle + PI); 
  popMatrix(); 
}

void draw(){ 

 for(int i=0;i<arcs.length;i++){
    Arc a = arcs[i];
    noFill();
    float cl = map(millis(), 0, 5.0, 0, 60);   
    float al = map(millis(), 0, 1.2, 0, 30);  
    float m = map(millis(), 0, 20000, 0, 30);    
    if(millis() < 20000){             
        stroke(cl, 99, 99, m); 
    } 
    else stroke(cl, 99, 99, al);    
    //stroke(a.c);
    smooth();
    semicircle(a.st+a.duration, height/2, a.st, height/2 );
 }
}

class Arc{
  
  int st, duration;
  color c;
  
  Arc(int st, int duration){
    this.st = st;
    this.duration = duration;
    c = color((int)random(255), (int)random(255), (int)random(255));    
  }
}

