class Arc
  
  attr_accessor :st, :duration, :c, :pitch, :note
  
  def initialize(st, duration, pitch, c =nil)
    @st = st
    @duration = duration
    @pitch = pitch   
    @c = c
  end
    
end
