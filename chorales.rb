require 'arc'
require 'note'

SCALE_FACTOR = 80
HEIGHT = 768
WIDTH = 1280
X_AXIS = HEIGHT/2 +50


def setup
  size WIDTH, HEIGHT
  background 0,0,0
  @arcs = []
  @colors = {}
  @notations = {}
  @current_choral = 0
  @selected = []
  read_chorales
  update_notes @current_choral
  puts "#{@chorales.size}"
  font = createFont("SansSerif", 20)
  textFont font
end

def semicircle xLeft, yLeft, xRight, yRight
  pushMatrix
    diameter = dist xLeft, yLeft, xRight, yRight
    angle = atan2(yRight - yLeft, xRight - xLeft)
    arc((xLeft + xRight)/2, (yLeft + yRight) / 2, diameter, diameter, angle, angle + PI)
  popMatrix
end


def draw
  
  background 0,0,0
  draw_labels
  draw_rectangles
  @arcs.each do |ar|
    noFill    
    stroke(ar.c)
    smooth
    semicircle(ar.st+ar.duration, X_AXIS, ar.st,X_AXIS )
    line(ar.st+ar.duration, X_AXIS+10, ar.st+ar.duration, X_AXIS+10+(ar.pitch*2))
    #Escreve a notação musical da nota
    #textSize 6
    #text("#{@notations[ar.pitch.to_s]}", ar.st+ar.duration,X_AXIS+10+ar.pitch+10)      
  end
  
end

def draw_labels
  fill 255
  textSize 10
  textAlign CENTER, TOP
  stroke 224
  strokeWeight 1
  @arcs.each do |ar|
    text("#{ar.st+ar.duration}", ar.st+ar.duration, X_AXIS)
    #text("#{ar.note}", ar.st+ar.duration, X_AXIS+5)    
  end
end

def draw_rectangles  
  @played = []
  y_pos = 700
  @notes.each{|n| @played << n.pitch}
  @played.uniq!.sort!
  @bar_width = @played.size*50
  
  @played.each_with_index do |n, i|
    c = @colors[n.to_s]
    #if played.include?(k)
      x_pos = (i+1)*50#map(n, played.first, played.last, 50, 50+bar_width )      
      fill c
      beginShape
      vertex x_pos,700
      vertex x_pos+50,700
      vertex x_pos+50,750
      vertex x_pos,750
      endShape
      fill 255
      textSize 10      
      textAlign CENTER, TOP
      text(@notations["#{n}"],x_pos+25,y_pos+25)  
    #end
  end
  
end
  


def update_notes(current = nil)
  
  if @selected.size > 0
    list = @selected  
  else
    @notes = @chorales[current] if current
    list = @notes
  end
  @arcs = []
  list.each do |note|   
    @arcs << Arc.new(note.st, note.duration, note.pitch, @colors[note.pitch.to_s])
  end
  puts @arcs.size
end


def read_chorales
  lines = []
  File.open("bach.lisp", "r") do |infile|
       while (line = infile.gets)
           lines << "#{line}" unless line.size == 1 #ignora linhas em branco
       end
  end
  @chorales = []
  
  lines.each do |choral|
    notes = []
    a = choral.split('st ')
    a[1..a.size-1].each do |st|
      notes << Note.new((st.split(')')[0]).to_i)
    end

    #pega a duration
    a = choral.split('dur ')
    a[1..a.size-1].each_with_index do |dur, index|  
      notes[index].duration = (dur.split(')')[0]).to_i * SCALE_FACTOR
    end
    
    #pega o pitch
    a = choral.split('pitch ')
    a[1..a.size-1].each_with_index do |pitch, index|  
      notes[index].pitch = (pitch.split(')')[0]).to_i #* (SCALE_FACTOR/50)
    end
    
    #pega a nota         
    File.open("notes", "r") do |infile|
      while (line = infile.gets)        
         @notations["#{line.split(' ')[0]}"] = "#{line.split(' ')[1]}"
         @colors["#{line.split(' ')[0]}"] = self.color(rand(255), rand(255), rand(255), 90)
      end
    end
   
    @chorales << notes
  end
  
end

def key_pressed
  if key == '['
    @current_choral-=1 unless @current_choral < 1    
  elsif key == ']'
    @current_choral+=1 unless @current_choral == @chorales.size-1
  end
  update_notes @current_choral
end


def mouse_pressed
    x = mouse_x
    y = mouse_y
    if (x > 50 && x < 50+@bar_width &&
        y > 700 && y < 700+50)
      find_note x  
    else
      @selected = []
      update_notes @current_choral
    end
end

def find_note(x)
  size = @bar_width/50  
  note = x/size/10
  @selected = @notes.select{|n| n.pitch == @played[note-1]} 
  update_notes @current_choral
end
